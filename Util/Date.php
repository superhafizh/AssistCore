<?php

namespace Assist\Core\Util;

/**
 * Mengubah tanggal menjadi instance dari DateTime.
 *
 * @param * $dTgl
 *
 * @return \DateTime
 */
function toDate($dTgl)
{
  if(is_object($dTgl) && get_class($dTgl) == 'DateTime') {
    return $dTgl;
  } else if(is_string($dTgl)) {
    return new \DateTime($dTgl);
  } else if(is_int($dTgl)) {
    return new \DateTime(date('Y-m-d',$dTgl));
  }
  return new \DateTime();
}

/**
 * Mendapatkan interval antara 2 tanggal
 *
 * @param * $dTglAwal
 * @param * $dTglAkhir
 *
 * @return \DateInterval
 */
function getInterval($dTglAwal, $dTglAkhir)
{
  $dTglAwal   = toDate($dTglAwal);
  $dTglAkhir  = toDate($dTglAkhir);
  return $dTglAwal->diff($dTglAkhir);
}

/**
 * Mendapatkan selisih hari antara 2 tanggal
 *
 * @param * $dTglAwal
 * @param * $dTglAkhir
 *
 * @return int
 */
function getKeHari($dTglAwal, $dTglAkhir)
{
  $interval = getInterval($dTglAwal, $dTglAkhir);
  return (intval($interval->format('%a')));
}

/**
 * Mendapatkan selisih bulan antara 2 tanggal
 *
 * @param * $dTglAwal
 * @param * $dTglAkhir
 *
 * @return int
 */
function getKeBulan($dTglAwal, $dTglAkhir)
{
  $interval = getInterval($dTglAwal, $dTglAkhir);
  return ($interval->m + ($interval->y * 12));
}

/**
 * Mendapatkan selisih tahun antara 2 tanggal
 *
 * @param * $dTglAwal
 * @param * $dTglAkhir
 *
 * @return int
 */
function getKeTahun($dTglAwal, $dTglAkhir)
{
  $interval = getInterval($dTglAwal, $dTglAkhir);
  return ($interval->y);
}
